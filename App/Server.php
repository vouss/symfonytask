<?php
    require_once('../class/DataBaseConnect.php');
    require_once('ElectricalMeasurements.php');

    $object = new ElectricalMeasurements();

    if($_POST['action'] == 'getAllReads') {
        echo json_encode($object->getAllReads());
    } elseif($_POST['action'] == 'filter') {
        echo json_encode($object->filter());
    }

?>