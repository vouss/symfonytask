<?php
    class ElectricalMeasurements {

        private $obj_db = null;
        private $read_array = array();
        private $reads = null;
        private $averagePerDay = null;

        function __construct()
        {
            $this->obj_db = new DataBaseConnect('mysql', 'localhost', 'symfonyLab', 'root', 'password1');
        }

        private function isAajax() {
            return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
                strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')? true : false;

        }

        private function isPost() {
            return ($_SERVER['REQUEST_METHOD'] == 'POST')? true : false;
        }

        public function getAllReads() {
            if($this->isAajax()) {
                if($this->isPost()) {
                    $sql1 = "SELECT `read`,`date` FROM electricity_meter_reads;";
                    $query = $this->obj_db->db_handle->prepare($sql1);
                    $query->execute();
                    $rows = $query->fetchAll(PDO::FETCH_ASSOC);

                    $this->rebuild($rows);
                    $this->calcAverage();
                    return $this->read_array;

                }
            }
        }
        private function rebuild($rows) {
            $i = 0;
            foreach($rows as $row) {
                $this->reads[$i] =  $row['read'];
                $this->read_array[0][$row['date']] = $row['read'];
                $i++;
            }

            // struktura: array('2014-01-01'=>0,'2014-01-30'=>149.90,'2014-02-15'=>222.22)
            //var_dump($this->read_array);
        }

        public function filter() {
            $sql1 = "SELECT * FROM electricity_meter_reads WHERE `date` BETWEEN '" .
                $_POST['data']['first'] . "' AND '". $_POST['data']['second']   . "';";
            $query = $this->obj_db->db_handle->prepare($sql1);
            $query->execute();
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);

            $sql2 = "SELECT * FROM electricity_meter_reads WHERE id < " .$rows[0]['id'] . " LIMIT 1;";
            $query2 = $this->obj_db->db_handle->prepare($sql2);
            $query2->execute();
            $rows2 = $query2->fetchAll(PDO::FETCH_ASSOC);

            //Warunek zadania: tablica asocjacyjna
            $all_rows = array_merge($rows2, $rows);
            $this->rebuild($all_rows);
            $this->calcAverage();
            return $this->read_array;
        }

        public function calcAverage() {
            $onlyDates = null;
            $tmp_array = array();
            $diffdays = null;
            $averages = null;


            $i = 0;
            foreach($this->read_array[0] as $key => $value) {
                $onlyDates[$i] = $key;
                $tmp_array[$i] = explode('-', $key);
                $i++;
            }

            for($j=0; $j < count($onlyDates)-1; $j++) {
                $tmp1 = new DateTime($onlyDates[$j]);
                if($j < count($onlyDates)-1) {
                    $tmp2 = new DateTime($onlyDates[$j+1]);
                }

                $interval = $tmp1->diff($tmp2);
                $diffdays[$j] = $interval->format('%a');
            }

            for($f=0,$k=1; $k < count($this->reads); $k++,$f++) {
                $averages[$f] = round($this->reads[$k]/$diffdays[$f], 2);
            }

            //var_dump($averages);
            $rang=null;
            for($i=0;$i<count($onlyDates)-1;$i++) {
                $begin = new DateTime( $onlyDates[$i]);
                $end = new DateTime( $onlyDates[$i+1]);
                $end = $end->modify( '+1 day' );
                $interval = new DateInterval('P1D');
                $rang[$i] = new DatePeriod($begin, $interval ,$end);
            }
//            var_dump($rang);

            $ended_array = array();
            $o = 0;
            for($j=0; $j<count($rang); $j++) {
                foreach($rang[$j] as $date){
                    $ended_array[$j][$o][0] = $date->format("Y-m-d");
                    $ended_array[$j][$o][1] = $averages[$j];
                    $o++;
                }
            }

//            var_dump($ended_array);
            //$ended_array -> dla każdego przedzialu ma strukture
            //array( '2015-01-08'=>10.0,'2015-01-09'=>10.0,'2015-01-10'=>10.0,'2015-01-11'=>5.0,'2015-01-12'=>5.0,'2015-01- 13'=>5.0,'2015-01-14'=>5.0,'2015-01-15'=>5.0)
            $this->read_array[1] = $ended_array;

        }

    }

?>