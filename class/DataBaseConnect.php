<?php

/**
 * Class DataBaseConnect
 * @package Db
 */
class DataBaseConnect {

    /**
     * @var null
     */
    private $db_type = null;
    /**
     * @var null
     */
    private $db_host = null;
    /**
     * @var null
     */
    private $db_name = null;
    /**
     * @var null
     */
    private $db_user = null;
    /**
     * @var null
     */
    private $db_pass = null;

    /**
     * @var null
     */
    public $db_handle = null;

    /**
     * @param $db_type
     * @param $db_host
     * @param $db_name
     * @param $db_user
     * @param $db_pass
     */
    function __construct($db_type, $db_host, $db_name, $db_user, $db_pass)
    {
        $this->db_type = $db_type;
        $this->db_host = $db_host;
        $this->db_name = $db_name;
        $this->db_user = $db_user;
        $this->db_pass = $db_pass;
        $this->connect();
    }

    /**
     * Method allow connect to db
     */
    public function connect() {

        try {
            $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
            $this->db_handle = new PDO($this->db_type .
                                        ':host=' . $this->db_host .
                                        ';dbname=' . $this->db_name, $this->db_user, $this->db_pass, $options);
            $this->db_handle->query("SET NAMES 'utf8'");
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



}