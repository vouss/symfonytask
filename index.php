<!doctype html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>symfonyLab by Marcin Boś</title>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="index.php" class="navbar-brand">SymfonyLab | Marcin Boś</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="https://bitbucket.org/vouss/symfonytask/overview" target="_blank"><span class="glyphicon glyphicon-paperclip"></span> Repository</a></li>
            </ul>

        </div>
    </div>
</div>
<div class="container" style="padding-top: 70px;">
    <div class="row"">
        <div class="col-md-6">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>READ (kWh)</th>
                        <th>DATE</th>
                    </tr>
                    </thead>
                    <tbody id="reads"></tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <form id="filter">
                Between
                <input type="text" class="input-sm" id="firstDate" placeholder="YYYY-MM-DD" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" required/> and
                <input type="text" class="input-sm" id="secondDate" placeholder="YYYY-MM-DD" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" required/>
                <input type="submit" class="btn btn-primary" value="Filter"/>
            </form>
            <div class="page-header">
                <h2 style="display: inline-block">Average per range</h2>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#AverageChart">
                    <span class="glyphicon glyphicon-stats" style="font-size: 30px;"></span>
                </button>
            </div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Day</th>
                    <th>Average</th>
                </tr>
                </thead>
                <tbody id="average"></tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" id="AverageChart" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:1200px">
        <div class="modal-content" style="padding: 10px;">
            <canvas id="myChart" width="1000" height="500"></canvas>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="charts/Chart.js/Chart.min.js"></script>
<script>
    $(function() {

        Array.prototype.mapProperty = function(property) {
            return this.map(function (obj) {
                return obj[property];
            });
        };

        Chart.defaults.global = {
            // Boolean - Whether to animate the chart
            animation: true,

            // Number - Number of animation steps
            animationSteps: 60,

            // String - Animation easing effect
            animationEasing: "easeOutQuart",

            // Boolean - If we should show the scale at all
            showScale: true,

            // Boolean - If we want to override with a hard coded scale
            scaleOverride: false,

            // ** Required if scaleOverride is true **
            // Number - The number of steps in a hard coded scale
            scaleSteps: null,
            // Number - The value jump in the hard coded scale
            scaleStepWidth: null,
            // Number - The scale starting value
            scaleStartValue: null,

            // String - Colour of the scale line
            scaleLineColor: "rgba(0,0,0,.1)",

            // Number - Pixel width of the scale line
            scaleLineWidth: 1,

            // Boolean - Whether to show labels on the scale
            scaleShowLabels: true,

            // Interpolated JS string - can access value
            scaleLabel: "<%=value%>",

            // Boolean - Whether the scale should stick to integers, not floats even if drawing space is there
            scaleIntegersOnly: true,

            // Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
            scaleBeginAtZero: false,

            // String - Scale label font declaration for the scale label
            scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

            // Number - Scale label font size in pixels
            scaleFontSize: 12,

            // String - Scale label font weight style
            scaleFontStyle: "normal",

            // String - Scale label font colour
            scaleFontColor: "#666",

            // Boolean - whether or not the chart should be responsive and resize when the browser does.
            responsive: false,

            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,

            // Boolean - Determines whether to draw tooltips on the canvas or not
            showTooltips: true,

            // Function - Determines whether to execute the customTooltips function instead of drawing the built in tooltips (See [Advanced - External Tooltips](#advanced-usage-custom-tooltips))
            customTooltips: false,

            // Array - Array of string names to attach tooltip events
            tooltipEvents: ["mousemove", "touchstart", "touchmove"],

            // String - Tooltip background colour
            tooltipFillColor: "rgba(0,0,0,0.8)",

            // String - Tooltip label font declaration for the scale label
            tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

            // Number - Tooltip label font size in pixels
            tooltipFontSize: 14,

            // String - Tooltip font weight style
            tooltipFontStyle: "normal",

            // String - Tooltip label font colour
            tooltipFontColor: "#fff",

            // String - Tooltip title font declaration for the scale label
            tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

            // Number - Tooltip title font size in pixels
            tooltipTitleFontSize: 14,

            // String - Tooltip title font weight style
            tooltipTitleFontStyle: "bold",

            // String - Tooltip title font colour
            tooltipTitleFontColor: "#fff",

            // Number - pixel width of padding around tooltip text
            tooltipYPadding: 6,

            // Number - pixel width of padding around tooltip text
            tooltipXPadding: 6,

            // Number - Size of the caret on the tooltip
            tooltipCaretSize: 8,

            // Number - Pixel radius of the tooltip border
            tooltipCornerRadius: 6,

            // Number - Pixel offset from point x to tooltip edge
            tooltipXOffset: 10,

            // String - Template string for single tooltips
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",

            // String - Template string for multiple tooltips
            multiTooltipTemplate: "<%= value %>",

            // Function - Will fire on animation progression.
            onAnimationProgress: function(){},

            // Function - Will fire on animation completion.
            onAnimationComplete: function(){}
        }
        Chart.defaults.global.responsive = true;
        var ctx = document.getElementById("myChart").getContext("2d");
        var data2 = {
            labels: [],
            datasets: []
        };
        var tmp1 = [];
        var tmp2 = [];

        var data = {
            'action' : 'getAllReads'
        };

        $.ajax({
            url: "App/Server.php",
            type: "POST",
            data: data,
            dataType: 'json'
        }).success(function (data) {
            var s = "";
            var x = "";

            $.each( data[0], function( key, value ) {
                s += "<tr><td>" + value + "</td><td>" + key + "</td></tr>";
            });


            $.each( data[1], function( key, value ) {
                for(dat in value) {
                    tmp2.push(value[dat][0]);
                    tmp1.push(parseInt(value[dat][1]));
                    x += "<tr><td>" + value[dat][0] +  "</td><td>" + value[dat][1] + " kWh</td></tr>";
                }

                var tmp3 = [
                    {
                        label: "Averages",
                        fillColor: "rgba(220,220,220,0.5)",
                        strokeColor: "rgba(220,220,220,0.8)",
                        highlightFill: "rgba(220,220,220,0.75)",
                        highlightStroke: "rgba(220,220,220,1)",
                        data: tmp1.mapProperty()
                    }
                ];

//                console.log(tmp2);
//                console.log(tmp3);
                data2.labels.push(tmp2);
                data2.datasets.push(tmp3);
            });

            $('#reads').html(s);
            $('#average').html(x);
            //var myBarChart = new Chart(ctx).Bar(data2, 0);
        });

        $("#filter").submit(function (event) {
            event.preventDefault();
            var s = "";
            var x = "";
            var data = {
                'action' : 'filter',
                'data' : {
                    'first' : $('#firstDate').val(),
                    'second' : $('#secondDate').val()
                }
            };

            $.ajax({
                url: "App/Server.php",
                type: "POST",
                data: data,
                dataType: 'json'
            }).success(function (data) {
                $('#reads').empty();
                $('#average').empty();

                $.each( data[0], function( key, value ) {
                    s += "<tr><td>" + value + "</td><td>" + key + "</td></tr>";
                });


                $.each( data[1], function( key, value ) {
                    for(dat in value) {
                        tmp2.push(value[dat][0]);
                        tmp1.push(parseInt(value[dat][1]));
                        x += "<tr><td>" + value[dat][0] +  "</td><td>" + value[dat][1] + " kWh</td></tr>";
                    }
                });

                    var tmp3 = [
                        {
                            label: "Averages",
                            fillColor: "rgba(220,220,220,0.5)",
                            strokeColor: "rgba(220,220,220,0.8)",
                            highlightFill: "rgba(220,220,220,0.75)",
                            highlightStroke: "rgba(220,220,220,1)",
                            data: tmp1
                        }
                    ];

//                console.log(tmp2);
//                console.log(tmp3);
                    data2.labels.push(tmp2);
                    data2.datasets.push(tmp3);

                $('#reads').html(s);
                $('#average').html(x);
                var myBarChart = new Chart(ctx).Bar(data2, 0);
            });
        });
        $('#calc').click(function () {
            $('#av').modal('show');
        });

    });
</script>
</body>
</html>